import React from "react";
import styled, { css } from "styled-components";
import { breakpoints, colours } from "../../common/theme";

const ServiceWrapper = styled.div`
	background-color: ${colours.primary};
	padding: 100px 0px 105px 0px;
`;

const InnerWrapper = styled.div`
	max-width: 1170px;
	width: 100%;
	padding: 10px;
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	flex-wrap: wrap;
	align-items: center;
	margin: 0 auto;
	position: relative;
`;

const Section = styled.section<{ single?: boolean }>`
	width: 100%;
	display: flex;
	flex-direction: column;
	max-width: 560px;
	${(p) =>
		p.single &&
		css`
			max-width: 100%;
			flex-direction: row;
			flex-wrap: wrap;
			justify-content: space-evenly;
		`}
`;

const ServiceTitleInfo = styled.div`
	margin-bottom: 20px;
	text-align: center;
`;

const TitleSpan = styled.span`
	color: ${colours.secondary};
	font-weight: 600;
	font-size: 16px;
	text-transform: uppercase;
	margin-bottom: 20px;
	display: block;
`;

const TitleHead = styled.h2`
	font-size: 62px;
	margin-bottom: 25px;
	font-weight: bold;
`;

const TitlePara = styled.p`
	font-size: 16px;
	line-height: 30px;
`;

const ServiceSpacer = styled.div`
	width: 10%;
	height: 3px;
	margin: 10px auto 50px;
	background-color: ${colours.secondary};
`;

const SingleService = styled.div`
	background-color: ${colours.primaryLight};
	border-radius: 20px;
	transition: 0.5s;
	padding: 50px 35px;
	margin-bottom: 50px;
	flex: 0 0 30.333333%;
	:hover {
		background-color: ${colours.secondary};
		i {
			color: white;
			transition: 0.8s linear;
			transform: rotateY(360deg);
		}
	}

	@media screen and (max-width: ${breakpoints.laptop}px) {
		flex: 0 0 47.333333%;
	}

	@media screen and (max-width: ${breakpoints.tablet}px) {
		flex: 0 0 97.333333%;
	}
`;

const ServiceIcon = styled.i`
	color: ${colours.secondary};
	font-size: 46px;
	display: inline-block;
	transition: 0.8s linear;
`;

const ServiceContent = styled.div`
	h5 {
		margin-top: 30px;
		margin-bottom: 20px;
		font-weight: 1000;
		font-size: 20px;
	}
	p {
		font-size: 16px;
	}
`;

const Services: React.FC = () => {
	return (
		<ServiceWrapper>
			<InnerWrapper>
				<Section>
					<ServiceTitleInfo>
						<TitleSpan>Services</TitleSpan>
						<TitleHead>My Services</TitleHead>
						<TitlePara>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum standard dummy text.</TitlePara>
					</ServiceTitleInfo>
					<ServiceSpacer />
				</Section>
				<Section single={true}>
					<SingleService>
						<ServiceIcon className="fa fa-github" />
						<ServiceContent>
							<h5>Web Development</h5>
							<p>Lorem Ipsum is simply dummy text of the printing typesetting industry. simply dummy</p>
						</ServiceContent>
					</SingleService>
					<SingleService>
						<ServiceIcon className="fa fa-codepen" />
						<ServiceContent>
							<h5>Web Design</h5>
							<p>Lorem Ipsum is simply dummy text of the printing typesetting industry. simply dummy</p>
						</ServiceContent>
					</SingleService>
					<SingleService>
						<ServiceIcon className="fa fa-file-video-o" />
						<ServiceContent>
							<h5>Video Editing</h5>
							<p>Lorem Ipsum is simply dummy text of the printing typesetting industry. simply dummy</p>
						</ServiceContent>
					</SingleService>
					<SingleService>
						<ServiceIcon className="fa fa-camera" />
						<ServiceContent>
							<h5>Photography</h5>
							<p>Lorem Ipsum is simply dummy text of the printing typesetting industry. simply dummy</p>
						</ServiceContent>
					</SingleService>
					<SingleService>
						<ServiceIcon className="fa fa-apple" />
						<ServiceContent>
							<h5>App Developing</h5>
							<p>Lorem Ipsum is simply dummy text of the printing typesetting industry. simply dummy</p>
						</ServiceContent>
					</SingleService>
					<SingleService>
						<ServiceIcon className="fa fa-yoast" />
						<ServiceContent>
							<h5>SEO Expert</h5>
							<p>Lorem Ipsum is simply dummy text of the printing typesetting industry. simply dummy</p>
						</ServiceContent>
					</SingleService>
				</Section>
			</InnerWrapper>
		</ServiceWrapper>
	);
};

export default Services;
