import React from "react";
import Intro from "./Intro";
import Services from "./Services";

const HomePage: React.FC = () => {
	return (
		<>
			<Intro />
			<Services />
		</>
	);
};

export default HomePage;
