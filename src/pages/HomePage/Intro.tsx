import { JSXElement } from "@babel/types";
import React from "react";
import styled, { css, keyframes } from "styled-components";
import { colMd12, colLg7, colLg5 } from "../../common/style";
import { breakpoints, colours } from "../../common/theme";

const ICON_ARRAY = ["fa fa-facebook", "fa fa-twitter", "fa fa-pinterest", "fa fa-instagram", "fa fa-behance"];

const IntroWrapper = styled.div`
	background-color: ${colours.primaryLight};
	background-image: url("https://themebing.com/wp/amike/wp-content/uploads/2021/02/bg.jpg");
	background-repeat: no-repeat;
	background-position: center center;
	height: auto;
	width: 100%;
	padding: 100px 0 0;
`;

const IntroContainer = styled.div`
	width: 100%;
	margin: 0 auto;
	padding: 0 15px;

	@media screen and (min-width: ${breakpoints.mobile}px) {
		max-width: 540px;
	}

	@media screen and (min-width: ${breakpoints.tablet}px) {
		max-width: 720px;
	}

	@media screen and (min-width: ${breakpoints.laptop}px) {
		max-width: 960px;
	}

	@media screen and (min-width: ${breakpoints.desktop}px) {
		max-width: 1170px;
	}
`;

const IntroRow = styled.div`
	display: flex;
	flex-wrap: wrap;
	margin-right: -15px;
	margin-left: -15px;
`;

const Col = css`
	position: relative;
	width: 100%;
	min-height: 1px;
	padding-right: 15px;
	padding-left: 15px;
`;

const IntroCol1 = styled.div`
	padding-top: 100px;
	${Col}
	${colMd12}
	${colLg7}
`;

const IntroCol2 = styled.div`
	${Col}
	${colLg5}
`;

const ImageWrapper = styled.div`
	margin: 0 auto;
	overflow: hidden;
`;

const Image = styled.img`
	height: auto;
	max-width: 100%;
	border: none;
	border-radius: 0;
	box-shadow: none;
`;

const UnorderedList = styled.ul`
	padding-left: 0;
	list-style: none;
	margin-bottom: 1rem;

	@media screen and (max-width: ${breakpoints.mobile}px) {
		text-align: center;
	}
`;

const ListItem = styled.li`
	display: inline-block;
	margin-right: 15px;
`;

const IconLink = styled.a`
	width: 50px;
	height: 50px;
	padding: 13px;
	border-radius: 50%;
	font-size: 20px;
	text-align: center;
	display: flex;
	justify-content: center;
	align-items: center;
	text-decoration: none;
	border: 2px solid ${colours.secondary};
	color: #fff;
	font-weight: 500;
	transition: 0.3s ease;
	background-color: transparent;
	cursor: pointer;
	:hover {
		background-color: ${colours.secondary};
	}
`;

const TextWrapper = styled.div`
	vertical-align: middle !important;
`;

const typing = keyframes`
	0% {
		width: 0;
	}
	100% {
		width: 100%;
	}
`;

const blinkCaret = keyframes`
	0%, 100% {
		border-color: white;
	}
	50% {
		border-color: transparent
	}
`;

const TextTypingHeader = styled.h1`
	margin-top: 50px;
	margin-bottom: 25px;
	font-weight: bold;
	font-size: 62px;
	overflow: hidden;
	border-right: 5px solid transparent;
	white-space: nowrap;
	word-wrap: break-word;
	display: flex;
	flex-wrap: wrap;
	color: #fff;
	animation: ${typing} 3.5s steps(30, end), ${blinkCaret} 0.5s step-end 7;

	@media screen and (max-width: ${breakpoints.laptop}px) {
		font-size: 54px;
	}

	@media screen and (max-width: ${breakpoints.tablet}px) {
		font-size: 46px;
	}

	@media screen and (max-width: ${breakpoints.mobile}px) {
		animation-name: none;
		white-space: normal;
		text-align: center;
	}
`;

const MoreInfo = styled.p`
	font-size: 26px;
	line-height: 40px;
	font-weight: 100;
	margin-bottom: 50px;

	@media screen and (max-width: ${breakpoints.mobile}px) {
		text-align: center;
	}
`;

const ButtonLink = styled.a`
	border: 2px solid ${colours.secondary};
	color: #fff;
	font-weight: 600;
	display: inline-block;
	padding: 13px 40px;
	border-radius: 50px;
	font-size: 15px;
	transition: 0.3s ease;
	background-color: transparent;
	cursor: pointer;
	margin-right: 15px;
	:hover {
		background-color: ${colours.secondary};
	}

	@media screen and (max-width: ${breakpoints.mobile}px) {
		margin-bottom: 10px;
		margin-right: 0;
	}
`;

const Icons = (props: Record<string, string[]>) => {
	return (
		<>
			{props.IconArray &&
				props.IconArray.map((Icon, index) => {
					return (
						<ListItem key={index}>
							<IconLink>
								<i className={Icon} />
							</IconLink>
						</ListItem>
					);
				})}
		</>
	);
};

const Intro: React.FC = () => {
	return (
		<IntroWrapper>
			<IntroContainer>
				<IntroRow>
					<IntroCol1>
						<UnorderedList>
							<Icons IconArray={ICON_ARRAY} />
						</UnorderedList>
						<TextWrapper>
							<TextTypingHeader>I am Jonathan Doe</TextTypingHeader>
							<MoreInfo>i'm Jonathan, professional web developer with long time experience in this field​</MoreInfo>
							<UnorderedList>
								<ListItem>
									<ButtonLink>My Portfolio</ButtonLink>
								</ListItem>
								<ListItem>
									<ButtonLink>
										<i className="fa fa-play" />
									</ButtonLink>
								</ListItem>
							</UnorderedList>
						</TextWrapper>
					</IntroCol1>
					<IntroCol2>
						<ImageWrapper>
							<Image
								width="508"
								height="729"
								src="https://themebing.com/wp/amike/wp-content/uploads/2019/12/man-01.png"
								alt=""
								loading="lazy"
								srcSet="https://themebing.com/wp/amike/wp-content/uploads/2019/12/man-01.png 508w, https://themebing.com/wp/amike/wp-content/uploads/2019/12/man-01-209x300.png 209w"
								sizes="(max-width: 508px) 100vw, 508px"
							/>
						</ImageWrapper>
					</IntroCol2>
				</IntroRow>
			</IntroContainer>
		</IntroWrapper>
	);
};

export default Intro;
