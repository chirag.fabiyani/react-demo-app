import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./pages/HomePage";
import PageNotFound from "./pages/PageNotFound";
import Header from "./widgets/Header";
import "./assets/css/custom.css";
import "font-awesome/css/font-awesome.min.css";

const AppInner: React.FC = () => {
	return (
		<>
			<Header />
			<Routes>
				<Route path="/" element={<HomePage />} />
				<Route path="*" element={<PageNotFound />} />
			</Routes>
		</>
	);
};

ReactDOM.render(
	<BrowserRouter>
		<AppInner />
	</BrowserRouter>,
	document.getElementById("root")
);
