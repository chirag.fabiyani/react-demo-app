const colours = {
	primary: "#1f2235",
	primaryLight: "#23263a",
	secondary: "#ff4a57",
};

const breakpoints = {
	desktop: 1400,
	laptop: 1024,
	tablet: 768,
	mobile: 576,
};

export { colours, breakpoints };
