import styled, { css } from "styled-components";
import { breakpoints } from "./theme";

export const colMd12 = css`
	@media screen and (min-width: ${breakpoints.tablet}px) {
		flex: 0 0 100%;
		max-width: 100%;
	}
`;

export const colLg7 = css`
	@media screen and (min-width: ${breakpoints.laptop}px) {
		flex: 0 0 58.333333%;
		max-width: 58.333333%;
	}
`;

export const colLg5 = css`
	@media screen and (min-width: ${breakpoints.laptop}px) {
		flex: 0 0 41.666667%;
		max-width: 41.666667%;
	}
`;
