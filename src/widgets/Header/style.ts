import styled, { css, keyframes } from "styled-components";
import { breakpoints, colours } from "../../common/theme";

const fadeInDown = keyframes`
	0%{
		opacity:0;
		transform:translateY(-20px);
	}
	100%{
		opacity:1;
		transform:translateY(0px);
	}
`;

export const HeadWrap = styled.div<{ showMenu: boolean; fixedTop: boolean }>`
	background-color: ${colours.primary};
	color: white;
	padding: 30px 0;
	display: flex;
	align-items: center;
	flex-direction: row;
	justify-content: space-between;
	flex-wrap: nowrap;

	${(p) =>
		p.fixedTop &&
		css`
			position: fixed;
			top: 0;
			right: 0;
			left: 0;
			z-index: 1030;
			background-color: ${colours.primaryLight};
			box-shadow: 0px 18px 40px -30px ${colours.primary};
			animation: ${fadeInDown} 1s ease-in-out 0s 1 normal none running;
		`}

	@media screen and (max-width:${breakpoints.mobile}px) {
		${(p) =>
			p.showMenu &&
			css`
				flex-wrap: wrap;
			`}
	}
`;

export const Logo = styled.div`
	font-family: Dancing Script, cursive;
	font-weight: bold;
	font-size: 45px;
	margin-left: 10%;
	width: 50%;
`;

export const NavWrapper = styled.div<{ showMenu: boolean }>`
	margin-right: 10%;
	display: flex;
	flex-direction: row;
	justify-content: flex-end;
	width: 50%;

	@media screen and (max-width: ${breakpoints.mobile}px) {
		display: ${(p) => (p.showMenu ? "block" : "none")};
		width: 100%;
	} ;
`;

export const HamburgerIcon = styled.div`
	@media screen and (max-width: ${breakpoints.mobile}px) {
		background-image: url("data:image/svg+xml;charset=utf8,%3Csvg viewBox='0 0 30 30' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath stroke='rgba(225, 225, 225, 1)' stroke-width='2' stroke-linecap='round' stroke-miterlimit='10' d='M4 7h22M4 15h22M4 23h22'/%3E%3C/svg%3E");
		background-repeat: no-repeat;
		background-position-x: right;
		background-position-y: center;
		height: 36px;
		width: 36px;
		margin-right: 10%;
	} ;
`;

export const Dropdown = styled.div`
	margin-left: 10%;
	cursor: pointer;
	position: relative;
	display: inline-block;

	@media screen and (max-width: ${breakpoints.mobile}px) {
		display: block;
		width: 100%;
	} ;
`;

export const DropdownButton = styled.button`
	background-color: transparent;
	color: white;
	padding: 16px;
	border: none;
	:hover {
		cursor: pointer;
		color: ${colours.secondary};
		+ .dropdown-content {
			display: block;
		}
	}

	@media screen and (max-width: ${breakpoints.mobile}px) {
		padding: 20px 0 0 0;
		::after {
			display: inline-block;
			width: 0;
			height: 0;
			margin-left: 0.255em;
			vertical-align: 0.255em;
			content: "";
			border-top: 0.3em solid;
			border-right: 0.3em solid transparent;
			border-bottom: 0;
			border-left: 0.3em solid transparent;
		}
	}
`;

export const DropdownContent = styled.div`
	display: none;
	position: absolute;
	background-color: ${colours.primaryLight};
	border-radius: 6px;
	min-width: 160px;
	border-radius: 1px solid rgba(0, 0, 0, 0.15);
	z-index: 1000;
`;

export const DropdownLink = styled.a`
	color: white;
	padding: 12px 16px;
	text-decoration: none;
	display: block;
	:hover {
		background-color: ${colours.secondary};
		transition: 0.4s ease;
	}
`;
