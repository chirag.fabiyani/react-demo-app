import React, { useEffect } from "react";
import { useState } from "react";
import { HeadWrap, Logo, NavWrapper, Dropdown, DropdownButton, DropdownContent, DropdownLink, HamburgerIcon } from "./style";

const Header: React.FC = () => {
	const [fixedTop, setFixedTop] = useState<boolean>(false);
	const [showMenu, setShowMenu] = useState<boolean>(false);

	useEffect(() => {
		document.addEventListener("scroll", onScroll);
	}, []);

	const onScroll: EventListener = (e: Event) => {
		if (window.pageYOffset > 120) {
			setFixedTop(true);
		} else {
			setFixedTop(false);
		}
	};

	const toggleNavbar: () => void = () => {
		setShowMenu(!showMenu);
	};

	return (
		<>
			<HeadWrap showMenu={showMenu} fixedTop={fixedTop}>
				<Logo>Blogs</Logo>
				<HamburgerIcon onClick={toggleNavbar} />
				<NavWrapper showMenu={showMenu}>
					<Dropdown>
						<DropdownButton>Categories</DropdownButton>
						<DropdownContent className="dropdown-content">
							<DropdownLink>Link 1</DropdownLink>
							<DropdownLink>Link 2</DropdownLink>
							<DropdownLink>Link 3</DropdownLink>
						</DropdownContent>
					</Dropdown>

					<Dropdown>
						<DropdownButton>Lists</DropdownButton>
						<DropdownContent className="dropdown-content">
							<DropdownLink>Link 1</DropdownLink>
							<DropdownLink>Link 2</DropdownLink>
							<DropdownLink>Link 3</DropdownLink>
						</DropdownContent>
					</Dropdown>
				</NavWrapper>
			</HeadWrap>
		</>
	);
};

export default Header;
